from typing import List
from models import Product
from fastapi import FastAPI, HTTPException
from pymongo import MongoClient
from bson import ObjectId

app = FastAPI()

# Connect to MongoDB
client = MongoClient("mongodb://172.17.0.1:27017/")
db = client["carpart"]
collection = db["stocks"]

# Check MongoDB connection
try:
    client.server_info()  # This statement raises an exception if the connection fails
    print("Connected to MongoDB!")
except Exception as e:
    print(f"Connection to MongoDB failed: {e}")

# Route to add one or more new products
@app.post("/add_products")
async def add_products(products: List[Product]):
    """
    Adds a list of products to the database.

    Parameters:
    - products (List[Product]): A list of Product objects containing the details of the products to be added.

    Returns:
    - dict: A dictionary containing the message "Products added successfully" and the IDs of the newly inserted products.
    """
    try:
        # Create a list of documents from Pydantic model data
        products_data = [
            {
                "name": product.name,
                "description": product.description,
                "quantity": product.quantity
            }
            for product in products
        ]

        # Insert new products into the database
        result = collection.insert_many(products_data)

        # Retrieve the IDs of the newly inserted products
        new_product_ids = [str(product_id) for product_id in result.inserted_ids]

        return {"message": "Products added successfully", "product_ids": new_product_ids}
    except Exception as e:
        # In case of an exception, return an HTTP 500 error with error details
        raise HTTPException(status_code=500, detail=str(e))


@app.put("/update_product/{product_id}")
async def update_product(product_id, product: Product):
    """
    Update a product in the MongoDB database.

    Parameters:
        product_id (str): The ID of the product to update.
        product (Product): The updated product object.

    Returns:
        dict: A dictionary with a message indicating the success of the update.
    """
    # Update a product in the MongoDB database
    collection.update_one({"_id": ObjectId(product_id)}, {"$set": product.dict()})
    return {"message": "Product updated successfully"}


@app.delete("/delete_product/{product_id}")
async def delete_product(product_id):
    """
    Delete a product from the MongoDB database.

    Parameters:
    - product_id (str): The ID of the product to be deleted.

    Returns:
    - dict: A dictionary containing the message "Product deleted successfully".
    """
    # Delete a product from the MongoDB database
    collection.delete_one({"_id": ObjectId(product_id)})
    return {"message": "Product deleted successfully"}


# Add routes to modify, delete, and access products in the database.
@app.get("/get_product/{product_id}")
async def get_product(product_id: str):
    """
    Retrieves a product from the database based on the provided product ID.

    Parameters:
        product_id (str): The ID of the product to retrieve.

    Returns:
        dict: A dictionary containing the retrieved product information.

    Raises:
        HTTPException: If the product is not found or if an internal server error occurs.
    """
    try:
        product_id_obj = ObjectId(product_id)

        product = collection.find_one({"_id": product_id_obj})

        if product is None:
            raise HTTPException(status_code=404, detail="Product not found")

        # Convert ObjectId to strings
        product["_id"] = str(product["_id"])

        return product
    
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))
    

# Retrieve all products from the database
@app.get("/get_all_products")
async def get_all_products():
    """
    Retrieves all products from the database.

    Returns:
        dict: A dictionary containing the products.
    
    Raises:
        HTTPException: If there is an error retrieving the products.
    """
    try:
        products = list(collection.find())

        # Convert ObjectId to strings
        for product in products:
            product["_id"] = str(product["_id"])

        return {"products": products}
    
    except Exception as e:
        raise HTTPException(status_code=500, detail=str(e))


# Run the application with Uvicorn
if __name__ == "__main__":
    import uvicorn
    uvicorn.run(app, host="127.0.0.1", port=8080)
