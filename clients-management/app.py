from fastapi import FastAPI, HTTPException
from pydantic import BaseModel
import os
import mysql.connector
from dotenv import load_dotenv

load_dotenv()

app = FastAPI()

# Model to represent client data
class Client(BaseModel):
    last_name: str
    first_name: str
    email: str
    order_count: int

db_host = os.getenv("DB_HOST")
db_user = os.getenv("DB_USER")
db_password = os.getenv("DB_PASSWORD")
db_name = os.getenv("DB_NAME")

print(db_host, db_user, db_password, db_name)

# Connecting to the MySQL database
mydb = mysql.connector.connect(
    host=db_host,
    user=db_user,
    password=db_password,
    database=db_name
)

# Route to add a client
@app.post("/add_client")
async def add_client(client: Client):
    """
    Inserts a new client into the database.

    Parameters:
        client (Client): The client object containing the client information.

    Returns:
        dict: A dictionary with a message indicating the success of the operation.
    """
    cursor = mydb.cursor()
    sql = "INSERT INTO clients (nom, prenom, email, nb_commandes) VALUES (%s, %s, %s, %s)"
    val = (client.last_name, client.first_name, client.email, client.order_count)
    cursor.execute(sql, val)
    mydb.commit()
    return {"message": "Client added successfully"}

@app.put("/update_client/{client_id}")
async def update_client(client_id: int, client: Client):
    """
    Updates a client record in the database.

    Parameters:
        client_id (int): The ID of the client to be updated.
        client (Client): The updated client object.

    Returns:
        dict: A dictionary containing a message indicating the success of the update operation.
    """
    cursor = mydb.cursor()
    sql = "UPDATE clients SET nom = %s, prenom = %s, email = %s, nb_commandes = %s WHERE id = %s"
    val = (client.last_name, client.first_name, client.email, client.order_count, client_id)
    cursor.execute(sql, val)
    mydb.commit()
    return {"message": f"Client record {client_id} updated successfully"}

@app.delete("/delete_client/{client_id}")
async def delete_client(client_id: int):
    """
    Delete a client from the database based on their client ID.

    Parameters:
        client_id (int): The ID of the client to be deleted.

    Returns:
        dict: A dictionary containing the message confirming the successful deletion of the client.
    """
    cursor = mydb.cursor()
    sql = "DELETE FROM clients WHERE id = %s"
    val = (client_id,)
    cursor.execute(sql, val)
    mydb.commit()
    return {"message": f"Client {client_id} deleted successfully"}

@app.get("/get_client/{client_id}")
async def get_client(client_id: int):
    cursor = mydb.cursor(dictionary=True)
    sql = "SELECT * FROM clients WHERE id = %s"
    """
    Retrieves a client from the database based on the provided client ID.

    Parameters:
        client_id (int): The ID of the client to retrieve.

    Returns:
        dict: A dictionary representing the client's information.

    Raises:
        HTTPException: If the client with the provided ID is not found in the database.
    """
    val = (client_id,)
    cursor.execute(sql, val)
    client = cursor.fetchone()
    if not client:
        raise HTTPException(status_code=404, detail="Client not found")
    return client

@app.get("/get_all_clients")
async def get_all_clients():
    """
    Retrieves all clients from the database.

    :return: A dictionary containing the list of clients.
    :rtype: dict
    """
    cursor = mydb.cursor(dictionary=True)
    sql = "SELECT * FROM clients"
    cursor.execute(sql)
    clients = cursor.fetchall()
    return {"clients": clients}
